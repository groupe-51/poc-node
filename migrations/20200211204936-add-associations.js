'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Order belongsTo Customer
    return queryInterface.addColumn(
        'Users', // name of Source model
        'roleId', // name of the key we're adding
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'Roles', // name of Target model
            key: 'id', // key in Target model that we're referencing
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        }
    )
        .then(() => {
          // Payment hasOne Order
          return queryInterface.addColumn(
              'Tracks', // name of Target model
              'userId', // name of the key we're adding
              {
                type: Sequelize.INTEGER,
                references: {
                  model: 'Users', // name of Source model
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
          );
        })
        .then(() => {
          // Order hasMany Product
          return queryInterface.addColumn(
              'InterestPoints', // name of Target model
              'trackId', // name of the key we're adding
              {
                type: Sequelize.INTEGER,
                references: {
                  model: 'Tracks', // name of Source model
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
          );
        })
        .then(() => {
          // Order hasMany Product
          return queryInterface.addColumn(
              'InterestPoints', // name of Target model
              'metaDataId', // name of the key we're adding
              {
                type: Sequelize.INTEGER,
                references: {
                  model: 'MetaData', // name of Source model
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
          );
        })
        .then(() => {
          // Order hasMany Product
          return queryInterface.addColumn(
              'InterestPoints', // name of Target model
              'userId', // name of the key we're adding
              {
                type: Sequelize.INTEGER,
                references: {
                  model: 'Users', // name of Source model
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
          );
        });
  },

  down: (queryInterface, Sequelize) => {
    // remove Order belongsTo Customer
    return queryInterface.removeColumn(
        'Users', // name of Source model
        'roleId', // name of the key we're adding
    )
        .then(() => {
          // remove Payment hasOne Order
          return queryInterface.removeColumn(
              'Tracks', // name of Target model
              'userId', // name of the key we're adding
          );
        })
        .then(() => {
          // remove Payment hasOne Order
          return queryInterface.removeColumn(
              'InterestPoints', // name of Target model
              'userId', // name of the key we're adding
          );
        })
        .then(() => {
          // remove Payment hasOne Order
          return queryInterface.removeColumn(
              'InterestPoints', // name of Target model
              'metaDataId', // name of the key we're adding
          );
        })
        .then(() => {
          // remove Order hasMany Product
          return queryInterface.removeColumn(
              'InterestPoints', // name of Target model
              'trackId', // name of the key we're adding
          );
        });
  }
};