var express = require('express');
var bodyParser = require('body-parser');
var apiRouter = require('./apiRouter').router;

//Instantiation du serveur
var server = express()

//Configuration de body parser
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());

//Configuration des routes 
server.get('/', function(request, response){
    response.setHeader('Content-Type', 'text/html');
    response.status(200).send('<h1 style="color:lightgreen; background: darkgrey; text-align: center; font-size: 44px">Serveur en marche</h1>');
})

server.use('/coincoins/', apiRouter);

//Lancement du serveur
server.listen(8081, function(){
    console.log('Serveur ON - Tout est ok')
});



