//Imports
var express = require('express');
var usersCtrl = require('./routes/usersCtrl');

// Routeur 
exports.router = (function() {
    var apiRouter = express.Router();

    //Users => routes
    //inscription
    apiRouter.route('/users/register/').post(usersCtrl.register);
    //authentification
    apiRouter.route('/users/login/').post(usersCtrl.login);
    //avoir le profil utilisateur
    apiRouter.route('/users/profil/').get(usersCtrl.getUserProfile);
    //update le profil utilisateur
    apiRouter.route('/users/profil/').put(usersCtrl.updateUserProfile);

return apiRouter; 

//Attention les parenthèses sont importantes => instantiation du router
})();