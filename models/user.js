'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    picture: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    enabled: DataTypes.BOOLEAN
  }, {});
  User.associate = function(models) {
    User.belongsTo(models.Role, {foreignKey: 'roleId'});
    User.hasMany(models.Track);
    User.hasMany(models.InterestPoint);
  };
  return User;
};