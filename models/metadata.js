'use strict';
module.exports = (sequelize, DataTypes) => {
  const MetaData = sequelize.define('MetaData', {
    content: DataTypes.STRING
  }, {});
  MetaData.associate = function(models) {
    MetaData.belongsTo(models.InterestPoint, {foreignKey: 'metadataId'});
  };
  return MetaData;
};