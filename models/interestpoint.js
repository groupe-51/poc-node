'use strict';
module.exports = (sequelize, DataTypes) => {
  const InterestPoint = sequelize.define('InterestPoint', {
    name: DataTypes.STRING,
    latitude: DataTypes.DOUBLE,
    longitude: DataTypes.DOUBLE,
    likes: DataTypes.INTEGER,
    enabled: DataTypes.BOOLEAN
  }, {});
  InterestPoint.associate = function(models) {
    InterestPoint.belongsTo(models.User, {foreignKey: 'userId'});
    InterestPoint.hasMany(models.Track);
    InterestPoint.hasMany(models.MetaData);
  };
  return InterestPoint;
};