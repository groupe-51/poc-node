'use strict';
module.exports = (sequelize, DataTypes) => {
  const Track = sequelize.define('Track', {
    name: DataTypes.STRING,
    coordinates: DataTypes.JSON,
    duration: DataTypes.TIME,
    distance: DataTypes.DOUBLE,
    difficulty: DataTypes.INTEGER,
    description: DataTypes.STRING,
    enabled: DataTypes.BOOLEAN
  }, {});
  Track.associate = function(models) {
    Track.belongsTo(models.User, {foreignKey: 'userId'});
    Track.hasMany(models.InterestPoint);
  };
  return Track;
};