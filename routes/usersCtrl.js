// Imports
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var models = require('../models');
var jwtUtils = require('../utils/jwt.utils');
var asyncLib = require('async');

//Constantes
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const PASSWORD_REGEX = /^(?=.*\d).{4,8}$/

//Routes
module.exports = {
    //Fonction d'enregistrement POST
    register: function(request, response) {

        //Paramètres
        var email = request.body.email;
        var username = request.body.username;
        var password = request.body.password;

        if(email == null || username ==  null || password == null){
            return response.status(400).json({'error':'Paramètres manquants'});
        }

        //REGEX MAIL / PSEUDO
        if(username.length >= 13 || username.length <= 4){
            return response.status(400).json({'error':'Votre pseudo doit être compris entre 5 et 12 caractères' });
        }

        if (!EMAIL_REGEX.test(email)) {
            return response.status(400).json({'error':'Mail invalide' });
        }

        if(!PASSWORD_REGEX.test(password)) {
            return response.status(400).json({'error':'Mot de passe invalide Minimum 4 - 8 caractères dont un nombre' });
        }

        models.User.findOne({
            attributes:['email'],
            where : {email : email}
        })
        .then(function(userFound) {
            if(!userFound){
                bcrypt.hash(password, 5, function(err, bcryptedPassword){
                    var newUser = models.User.create({
                        email: email,
                        username: username,
                        password : bcryptedPassword
                    })
                    .then(function(newUser) {
                        return response.status(201).json({
                            'userId': newUser.id
                        })
                    })
                    .catch(function(err) {
                        return response.status(500).json({'error':'Impossible de créer cet utilisateur : Erreur 500 '})
                    });
                });
            } else {
                return response.status(409).json({'error': 'Cet utilisateur existe déjà : Erreur 409'});
            }
        })
        .catch(function(err){
            console.log(err);
            return response.status(500).json({'error':'Impossible de verifier cet utilisateur : Erreur 500 '});
        });

    },
    //Fonction authentification POST
    login: function(request, response) {

        //Params
        var email = request.body.email;
        var password = request.body.password;

        if(email==null||password==null){
            return response.status(400).json({'error':'il manque des informations de connexion'});
        }

        models.User.findOne({
            where: { email: email}
        })
        .then(function(userFound) {
            if(userFound){
                bcrypt.compare(password, userFound.password, function(errBycrypt, resBycrypt) {
                    if(resBycrypt) {

                        return response.status(200).json({
                            'userId': userFound.id,
                            'token':jwtUtils.generateTokenForUser(userFound)
                        })
                    }
                })
            } else {
                return response.status(404).json({'error':'Cet utilisateur n ai pas enregistré: Erreur 404'});
            }
        })
        .catch(function(errBycrypt) {
            return response.status(500).json({'error':'impossible de contacter le serveur : Erreur 500 : '+ errBycrypt});
        })
    },

    //Fonction récupérer info utilisateur GET
    getUserProfile: function(request, response) {

        //Params
        var headerAuth = request.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);

        if(userId<0)
            return response.status(400).json({'error':'Token erroné'+userId});

        models.User.findOne({
            attributes: ['ID', 'email', 'username'],
            where: {id: userId}
        })
        .then(function(user) {
            if(user) {
                response.status(201).json(user);
            }
            else {
                response.status(404).json({'error':'Utilisateur non trouvé'})
            }
        })
        .catch(function(err) {
            response.status(500).json({'error': 'impossible de trouver cet utilisateur '+err})
        });

    },
    
    //Fonction update info utilisateur PUT
    updateUserProfile: function(request, response) {

        //Params
        var headerAuth = request.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);

        var username = request.body.username;

        asyncLib.waterfall([
            function(done) {
                models.User.findOne({
                    attributes:['id','username'],
                    where:{id:userId}
                }).then(function(userFound){
                    done(null, userFound);
                }).catch(function(err) {
                    return response.status(500).json({'error':'impossible de verifier cet utilisateur : Erreur 500'})
                })
            },
            function(userFound, done) {

                if(userFound) {
                        userFound.update({
                            username:(username ? username : userFound.username)
                        }).then(function(){
                            done(userFound);
                        }).catch(function(err) {
                          return response.status(500).json({'error':'impossible de modifier cet utilisateur pour le moment'})
                    })
                } else {
                    response.status(404).json({'error':'Utilisateur introuvable'})
                }
            },
            function(userFound) {
                if(userFound){
                    return response.status(201).json(userFound);
                }
                else {
                    return response.status(500).json({'error':'impossible de modifier cet utilisateur pour le moment'})
                }
            }
    
        ], function(){},function(err){return response.status(500).json({'error':'Une erreur est survenue : Erreur 500'})});

        request.setTimeout(600);
    }
}